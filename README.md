# luatest

Polycode lua experiment -- a game called "Unplayable Asteroids". Self-explanatory. You are not granted any rights to this code, but feel free to contact me if you want some.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/luatest).**
